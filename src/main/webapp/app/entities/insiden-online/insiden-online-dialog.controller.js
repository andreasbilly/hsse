(function() {
    'use strict';

    angular
        .module('hsseApplicationApp')
        .controller('InsidenOnlineDialogController', InsidenOnlineDialogController);

    InsidenOnlineDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'InsidenOnline'];

    function InsidenOnlineDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, InsidenOnline) {
        var vm = this;

        vm.insidenOnline = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.insidenOnline.id !== null) {
                InsidenOnline.update(vm.insidenOnline, onSaveSuccess, onSaveError);
            } else {
                InsidenOnline.save(vm.insidenOnline, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('hsseApplicationApp:insidenOnlineUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.ditetapkanTanggal = false;
        vm.datePickerOpenStatus.tanggalKejadian = false;
        vm.datePickerOpenStatus.jam = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
