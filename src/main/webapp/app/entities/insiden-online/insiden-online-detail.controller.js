(function() {
    'use strict';

    angular
        .module('hsseApplicationApp')
        .controller('InsidenOnlineDetailController', InsidenOnlineDetailController);

    InsidenOnlineDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'InsidenOnline'];

    function InsidenOnlineDetailController($scope, $rootScope, $stateParams, previousState, entity, InsidenOnline) {
        var vm = this;

        vm.insidenOnline = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('hsseApplicationApp:insidenOnlineUpdate', function(event, result) {
            vm.insidenOnline = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
