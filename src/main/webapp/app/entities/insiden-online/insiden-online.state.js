(function() {
    'use strict';

    angular
        .module('hsseApplicationApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('insiden-online', {
            parent: 'entity',
            url: '/insiden-online?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'InsidenOnlines'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/insiden-online/insiden-onlines.html',
                    controller: 'InsidenOnlineController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
            }
        })
        .state('insiden-online-detail', {
            parent: 'insiden-online',
            url: '/insiden-online/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'InsidenOnline'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/insiden-online/insiden-online-detail.html',
                    controller: 'InsidenOnlineDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'InsidenOnline', function($stateParams, InsidenOnline) {
                    return InsidenOnline.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'insiden-online',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('insiden-online-detail.edit', {
            parent: 'insiden-online-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/insiden-online/insiden-online-dialog.html',
                    controller: 'InsidenOnlineDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['InsidenOnline', function(InsidenOnline) {
                            return InsidenOnline.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('insiden-online.new', {
            parent: 'insiden-online',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/insiden-online/insiden-online-dialog.html',
                    controller: 'InsidenOnlineDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                ubahKe: null,
                                ditetapkanTanggal: null,
                                mulaiTerjadi: null,
                                lokasiKecelakaan: null,
                                tanggalKejadian: null,
                                namaKorban: null,
                                cideraKerusakan: null,
                                dampakKerugian: null,
                                department: null,
                                jam: null,
                                bagianTubuhTerluka: null,
                                penyebabCidera: null,
                                frekuensiTerjadi: null,
                                bagaimanaTerjadi: null,
                                penyebabKejadianTindakan: null,
                                penyebabFaktorManusia: null,
                                intervensiPencegahan: null,
                                tindakanMengeliminasi: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('insiden-online', null, { reload: 'insiden-online' });
                }, function() {
                    $state.go('insiden-online');
                });
            }]
        })
        .state('insiden-online.edit', {
            parent: 'insiden-online',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/insiden-online/insiden-online-dialog.html',
                    controller: 'InsidenOnlineDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['InsidenOnline', function(InsidenOnline) {
                            return InsidenOnline.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('insiden-online', null, { reload: 'insiden-online' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('insiden-online.delete', {
            parent: 'insiden-online',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/insiden-online/insiden-online-delete-dialog.html',
                    controller: 'InsidenOnlineDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['InsidenOnline', function(InsidenOnline) {
                            return InsidenOnline.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('insiden-online', null, { reload: 'insiden-online' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
