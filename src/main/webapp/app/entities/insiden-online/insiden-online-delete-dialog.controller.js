(function() {
    'use strict';

    angular
        .module('hsseApplicationApp')
        .controller('InsidenOnlineDeleteController',InsidenOnlineDeleteController);

    InsidenOnlineDeleteController.$inject = ['$uibModalInstance', 'entity', 'InsidenOnline'];

    function InsidenOnlineDeleteController($uibModalInstance, entity, InsidenOnline) {
        var vm = this;

        vm.insidenOnline = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            InsidenOnline.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
