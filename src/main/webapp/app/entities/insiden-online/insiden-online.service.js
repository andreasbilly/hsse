(function() {
    'use strict';
    angular
        .module('hsseApplicationApp')
        .factory('InsidenOnline', InsidenOnline);

    InsidenOnline.$inject = ['$resource', 'DateUtils'];

    function InsidenOnline ($resource, DateUtils) {
        var resourceUrl =  'api/insiden-onlines/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.ditetapkanTanggal = DateUtils.convertLocalDateFromServer(data.ditetapkanTanggal);
                        data.tanggalKejadian = DateUtils.convertLocalDateFromServer(data.tanggalKejadian);
                        data.jam = DateUtils.convertDateTimeFromServer(data.jam);
                    }
                    return data;
                }
            },
            'update': {
                method: 'PUT',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.ditetapkanTanggal = DateUtils.convertLocalDateToServer(copy.ditetapkanTanggal);
                    copy.tanggalKejadian = DateUtils.convertLocalDateToServer(copy.tanggalKejadian);
                    return angular.toJson(copy);
                }
            },
            'save': {
                method: 'POST',
                transformRequest: function (data) {
                    var copy = angular.copy(data);
                    copy.ditetapkanTanggal = DateUtils.convertLocalDateToServer(copy.ditetapkanTanggal);
                    copy.tanggalKejadian = DateUtils.convertLocalDateToServer(copy.tanggalKejadian);
                    return angular.toJson(copy);
                }
            }
        });
    }
})();
