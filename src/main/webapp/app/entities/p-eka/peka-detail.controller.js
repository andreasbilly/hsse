(function() {
    'use strict';

    angular
        .module('hsseApplicationApp')
        .controller('PEKADetailController', PEKADetailController);

    PEKADetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'PEKA'];

    function PEKADetailController($scope, $rootScope, $stateParams, previousState, entity, PEKA) {
        var vm = this;

        vm.pEKA = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('hsseApplicationApp:pEKAUpdate', function(event, result) {
            vm.pEKA = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
