(function() {
    'use strict';

    angular
        .module('hsseApplicationApp')
        .controller('PEKADialogController', PEKADialogController);

    PEKADialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'PEKA'];

    function PEKADialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, PEKA) {
        var vm = this;

        vm.pEKA = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.pEKA.id !== null) {
                PEKA.update(vm.pEKA, onSaveSuccess, onSaveError);
            } else {
                PEKA.save(vm.pEKA, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('hsseApplicationApp:pEKAUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.dateTime = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
