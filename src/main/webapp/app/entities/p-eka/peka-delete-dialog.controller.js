(function() {
    'use strict';

    angular
        .module('hsseApplicationApp')
        .controller('PEKADeleteController',PEKADeleteController);

    PEKADeleteController.$inject = ['$uibModalInstance', 'entity', 'PEKA'];

    function PEKADeleteController($uibModalInstance, entity, PEKA) {
        var vm = this;

        vm.pEKA = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            PEKA.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
