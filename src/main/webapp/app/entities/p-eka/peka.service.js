(function() {
    'use strict';
    angular
        .module('hsseApplicationApp')
        .factory('PEKA', PEKA);

    PEKA.$inject = ['$resource', 'DateUtils'];

    function PEKA ($resource, DateUtils) {
        var resourceUrl =  'api/p-ekas/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.dateTime = DateUtils.convertDateTimeFromServer(data.dateTime);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
