(function() {
    'use strict';

    angular
        .module('hsseApplicationApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('peka', {
            parent: 'entity',
            url: '/peka?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'PEKAS'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/p-eka/p-ekas.html',
                    controller: 'PEKAController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
            }
        })
        .state('peka-detail', {
            parent: 'peka',
            url: '/peka/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'PEKA'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/p-eka/peka-detail.html',
                    controller: 'PEKADetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'PEKA', function($stateParams, PEKA) {
                    return PEKA.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'peka',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('peka-detail.edit', {
            parent: 'peka-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/p-eka/peka-dialog.html',
                    controller: 'PEKADialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['PEKA', function(PEKA) {
                            return PEKA.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('peka.new', {
            parent: 'peka',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/p-eka/peka-dialog.html',
                    controller: 'PEKADialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                dateTime: null,
                                insiden: null,
                                lokasi: null,
                                potensiBahaya: null,
                                intervensi: null,
                                hasilPengamatan: null,
                                tindakanLangsung: null,
                                infoTambahan: null,
                                intervensiMencegah: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('peka', null, { reload: 'peka' });
                }, function() {
                    $state.go('peka');
                });
            }]
        })
        .state('peka.edit', {
            parent: 'peka',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/p-eka/peka-dialog.html',
                    controller: 'PEKADialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['PEKA', function(PEKA) {
                            return PEKA.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('peka', null, { reload: 'peka' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('peka.delete', {
            parent: 'peka',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/p-eka/peka-delete-dialog.html',
                    controller: 'PEKADeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['PEKA', function(PEKA) {
                            return PEKA.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('peka', null, { reload: 'peka' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
