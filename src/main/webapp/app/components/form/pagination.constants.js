(function() {
    'use strict';

    angular
        .module('hsseApplicationApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
