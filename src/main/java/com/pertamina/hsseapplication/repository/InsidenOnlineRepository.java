package com.pertamina.hsseapplication.repository;

import com.pertamina.hsseapplication.domain.InsidenOnline;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the InsidenOnline entity.
 */
@SuppressWarnings("unused")
public interface InsidenOnlineRepository extends JpaRepository<InsidenOnline,Long> {

}
