package com.pertamina.hsseapplication.repository;

import com.pertamina.hsseapplication.domain.PEKA;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the PEKA entity.
 */
@SuppressWarnings("unused")
public interface PEKARepository extends JpaRepository<PEKA,Long> {

}
