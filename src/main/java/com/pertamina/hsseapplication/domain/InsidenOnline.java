package com.pertamina.hsseapplication.domain;


import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A InsidenOnline.
 */
@Entity
@Table(name = "insiden_online")
public class InsidenOnline implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "ubah_ke")
    private String ubahKe;

    @Column(name = "ditetapkan_tanggal")
    private LocalDate ditetapkanTanggal;

    @Column(name = "mulai_terjadi")
    private String mulaiTerjadi;

    @Column(name = "lokasi_kecelakaan")
    private String lokasiKecelakaan;

    @Column(name = "tanggal_kejadian")
    private LocalDate tanggalKejadian;

    @Column(name = "nama_korban")
    private String namaKorban;

    @Column(name = "cidera_kerusakan")
    private String cideraKerusakan;

    @Column(name = "dampak_kerugian")
    private String dampakKerugian;

    @Column(name = "department")
    private String department;

    @Column(name = "jam")
    private ZonedDateTime jam;

    @Column(name = "bagian_tubuh_terluka")
    private String bagianTubuhTerluka;

    @Column(name = "penyebab_cidera")
    private String penyebabCidera;

    @Column(name = "frekuensi_terjadi")
    private String frekuensiTerjadi;

    @Column(name = "bagaimana_terjadi")
    private String bagaimanaTerjadi;

    @Column(name = "penyebab_kejadian_tindakan")
    private String penyebabKejadianTindakan;

    @Column(name = "penyebab_faktor_manusia")
    private String penyebabFaktorManusia;

    @Column(name = "intervensi_pencegahan")
    private String intervensiPencegahan;

    @Column(name = "tindakan_mengeliminasi")
    private String tindakanMengeliminasi;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUbahKe() {
        return ubahKe;
    }

    public InsidenOnline ubahKe(String ubahKe) {
        this.ubahKe = ubahKe;
        return this;
    }

    public void setUbahKe(String ubahKe) {
        this.ubahKe = ubahKe;
    }

    public LocalDate getDitetapkanTanggal() {
        return ditetapkanTanggal;
    }

    public InsidenOnline ditetapkanTanggal(LocalDate ditetapkanTanggal) {
        this.ditetapkanTanggal = ditetapkanTanggal;
        return this;
    }

    public void setDitetapkanTanggal(LocalDate ditetapkanTanggal) {
        this.ditetapkanTanggal = ditetapkanTanggal;
    }

    public String getMulaiTerjadi() {
        return mulaiTerjadi;
    }

    public InsidenOnline mulaiTerjadi(String mulaiTerjadi) {
        this.mulaiTerjadi = mulaiTerjadi;
        return this;
    }

    public void setMulaiTerjadi(String mulaiTerjadi) {
        this.mulaiTerjadi = mulaiTerjadi;
    }

    public String getLokasiKecelakaan() {
        return lokasiKecelakaan;
    }

    public InsidenOnline lokasiKecelakaan(String lokasiKecelakaan) {
        this.lokasiKecelakaan = lokasiKecelakaan;
        return this;
    }

    public void setLokasiKecelakaan(String lokasiKecelakaan) {
        this.lokasiKecelakaan = lokasiKecelakaan;
    }

    public LocalDate getTanggalKejadian() {
        return tanggalKejadian;
    }

    public InsidenOnline tanggalKejadian(LocalDate tanggalKejadian) {
        this.tanggalKejadian = tanggalKejadian;
        return this;
    }

    public void setTanggalKejadian(LocalDate tanggalKejadian) {
        this.tanggalKejadian = tanggalKejadian;
    }

    public String getNamaKorban() {
        return namaKorban;
    }

    public InsidenOnline namaKorban(String namaKorban) {
        this.namaKorban = namaKorban;
        return this;
    }

    public void setNamaKorban(String namaKorban) {
        this.namaKorban = namaKorban;
    }

    public String getCideraKerusakan() {
        return cideraKerusakan;
    }

    public InsidenOnline cideraKerusakan(String cideraKerusakan) {
        this.cideraKerusakan = cideraKerusakan;
        return this;
    }

    public void setCideraKerusakan(String cideraKerusakan) {
        this.cideraKerusakan = cideraKerusakan;
    }

    public String getDampakKerugian() {
        return dampakKerugian;
    }

    public InsidenOnline dampakKerugian(String dampakKerugian) {
        this.dampakKerugian = dampakKerugian;
        return this;
    }

    public void setDampakKerugian(String dampakKerugian) {
        this.dampakKerugian = dampakKerugian;
    }

    public String getDepartment() {
        return department;
    }

    public InsidenOnline department(String department) {
        this.department = department;
        return this;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public ZonedDateTime getJam() {
        return jam;
    }

    public InsidenOnline jam(ZonedDateTime jam) {
        this.jam = jam;
        return this;
    }

    public void setJam(ZonedDateTime jam) {
        this.jam = jam;
    }

    public String getBagianTubuhTerluka() {
        return bagianTubuhTerluka;
    }

    public InsidenOnline bagianTubuhTerluka(String bagianTubuhTerluka) {
        this.bagianTubuhTerluka = bagianTubuhTerluka;
        return this;
    }

    public void setBagianTubuhTerluka(String bagianTubuhTerluka) {
        this.bagianTubuhTerluka = bagianTubuhTerluka;
    }

    public String getPenyebabCidera() {
        return penyebabCidera;
    }

    public InsidenOnline penyebabCidera(String penyebabCidera) {
        this.penyebabCidera = penyebabCidera;
        return this;
    }

    public void setPenyebabCidera(String penyebabCidera) {
        this.penyebabCidera = penyebabCidera;
    }

    public String getFrekuensiTerjadi() {
        return frekuensiTerjadi;
    }

    public InsidenOnline frekuensiTerjadi(String frekuensiTerjadi) {
        this.frekuensiTerjadi = frekuensiTerjadi;
        return this;
    }

    public void setFrekuensiTerjadi(String frekuensiTerjadi) {
        this.frekuensiTerjadi = frekuensiTerjadi;
    }

    public String getBagaimanaTerjadi() {
        return bagaimanaTerjadi;
    }

    public InsidenOnline bagaimanaTerjadi(String bagaimanaTerjadi) {
        this.bagaimanaTerjadi = bagaimanaTerjadi;
        return this;
    }

    public void setBagaimanaTerjadi(String bagaimanaTerjadi) {
        this.bagaimanaTerjadi = bagaimanaTerjadi;
    }

    public String getPenyebabKejadianTindakan() {
        return penyebabKejadianTindakan;
    }

    public InsidenOnline penyebabKejadianTindakan(String penyebabKejadianTindakan) {
        this.penyebabKejadianTindakan = penyebabKejadianTindakan;
        return this;
    }

    public void setPenyebabKejadianTindakan(String penyebabKejadianTindakan) {
        this.penyebabKejadianTindakan = penyebabKejadianTindakan;
    }

    public String getPenyebabFaktorManusia() {
        return penyebabFaktorManusia;
    }

    public InsidenOnline penyebabFaktorManusia(String penyebabFaktorManusia) {
        this.penyebabFaktorManusia = penyebabFaktorManusia;
        return this;
    }

    public void setPenyebabFaktorManusia(String penyebabFaktorManusia) {
        this.penyebabFaktorManusia = penyebabFaktorManusia;
    }

    public String getIntervensiPencegahan() {
        return intervensiPencegahan;
    }

    public InsidenOnline intervensiPencegahan(String intervensiPencegahan) {
        this.intervensiPencegahan = intervensiPencegahan;
        return this;
    }

    public void setIntervensiPencegahan(String intervensiPencegahan) {
        this.intervensiPencegahan = intervensiPencegahan;
    }

    public String getTindakanMengeliminasi() {
        return tindakanMengeliminasi;
    }

    public InsidenOnline tindakanMengeliminasi(String tindakanMengeliminasi) {
        this.tindakanMengeliminasi = tindakanMengeliminasi;
        return this;
    }

    public void setTindakanMengeliminasi(String tindakanMengeliminasi) {
        this.tindakanMengeliminasi = tindakanMengeliminasi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        InsidenOnline insidenOnline = (InsidenOnline) o;
        if (insidenOnline.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, insidenOnline.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "InsidenOnline{" +
            "id=" + id +
            ", ubahKe='" + ubahKe + "'" +
            ", ditetapkanTanggal='" + ditetapkanTanggal + "'" +
            ", mulaiTerjadi='" + mulaiTerjadi + "'" +
            ", lokasiKecelakaan='" + lokasiKecelakaan + "'" +
            ", tanggalKejadian='" + tanggalKejadian + "'" +
            ", namaKorban='" + namaKorban + "'" +
            ", cideraKerusakan='" + cideraKerusakan + "'" +
            ", dampakKerugian='" + dampakKerugian + "'" +
            ", department='" + department + "'" +
            ", jam='" + jam + "'" +
            ", bagianTubuhTerluka='" + bagianTubuhTerluka + "'" +
            ", penyebabCidera='" + penyebabCidera + "'" +
            ", frekuensiTerjadi='" + frekuensiTerjadi + "'" +
            ", bagaimanaTerjadi='" + bagaimanaTerjadi + "'" +
            ", penyebabKejadianTindakan='" + penyebabKejadianTindakan + "'" +
            ", penyebabFaktorManusia='" + penyebabFaktorManusia + "'" +
            ", intervensiPencegahan='" + intervensiPencegahan + "'" +
            ", tindakanMengeliminasi='" + tindakanMengeliminasi + "'" +
            '}';
    }
}
