package com.pertamina.hsseapplication.domain;


import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A PEKA.
 */
@Entity
@Table(name = "peka")
public class PEKA implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "date_time")
    private ZonedDateTime dateTime;

    @Column(name = "insiden")
    private String insiden;

    @Column(name = "lokasi")
    private String lokasi;

    @Column(name = "potensi_bahaya")
    private String potensiBahaya;

    @Column(name = "intervensi")
    private String intervensi;

    @Column(name = "hasil_pengamatan")
    private String hasilPengamatan;

    @Column(name = "tindakan_langsung")
    private String tindakanLangsung;

    @Column(name = "info_tambahan")
    private String infoTambahan;

    @Column(name = "intervensi_mencegah")
    private String intervensiMencegah;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getDateTime() {
        return dateTime;
    }

    public PEKA dateTime(ZonedDateTime dateTime) {
        this.dateTime = dateTime;
        return this;
    }

    public void setDateTime(ZonedDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public String getInsiden() {
        return insiden;
    }

    public PEKA insiden(String insiden) {
        this.insiden = insiden;
        return this;
    }

    public void setInsiden(String insiden) {
        this.insiden = insiden;
    }

    public String getLokasi() {
        return lokasi;
    }

    public PEKA lokasi(String lokasi) {
        this.lokasi = lokasi;
        return this;
    }

    public void setLokasi(String lokasi) {
        this.lokasi = lokasi;
    }

    public String getPotensiBahaya() {
        return potensiBahaya;
    }

    public PEKA potensiBahaya(String potensiBahaya) {
        this.potensiBahaya = potensiBahaya;
        return this;
    }

    public void setPotensiBahaya(String potensiBahaya) {
        this.potensiBahaya = potensiBahaya;
    }

    public String getIntervensi() {
        return intervensi;
    }

    public PEKA intervensi(String intervensi) {
        this.intervensi = intervensi;
        return this;
    }

    public void setIntervensi(String intervensi) {
        this.intervensi = intervensi;
    }

    public String getHasilPengamatan() {
        return hasilPengamatan;
    }

    public PEKA hasilPengamatan(String hasilPengamatan) {
        this.hasilPengamatan = hasilPengamatan;
        return this;
    }

    public void setHasilPengamatan(String hasilPengamatan) {
        this.hasilPengamatan = hasilPengamatan;
    }

    public String getTindakanLangsung() {
        return tindakanLangsung;
    }

    public PEKA tindakanLangsung(String tindakanLangsung) {
        this.tindakanLangsung = tindakanLangsung;
        return this;
    }

    public void setTindakanLangsung(String tindakanLangsung) {
        this.tindakanLangsung = tindakanLangsung;
    }

    public String getInfoTambahan() {
        return infoTambahan;
    }

    public PEKA infoTambahan(String infoTambahan) {
        this.infoTambahan = infoTambahan;
        return this;
    }

    public void setInfoTambahan(String infoTambahan) {
        this.infoTambahan = infoTambahan;
    }

    public String getIntervensiMencegah() {
        return intervensiMencegah;
    }

    public PEKA intervensiMencegah(String intervensiMencegah) {
        this.intervensiMencegah = intervensiMencegah;
        return this;
    }

    public void setIntervensiMencegah(String intervensiMencegah) {
        this.intervensiMencegah = intervensiMencegah;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PEKA pEKA = (PEKA) o;
        if (pEKA.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, pEKA.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "PEKA{" +
            "id=" + id +
            ", dateTime='" + dateTime + "'" +
            ", insiden='" + insiden + "'" +
            ", lokasi='" + lokasi + "'" +
            ", potensiBahaya='" + potensiBahaya + "'" +
            ", intervensi='" + intervensi + "'" +
            ", hasilPengamatan='" + hasilPengamatan + "'" +
            ", tindakanLangsung='" + tindakanLangsung + "'" +
            ", infoTambahan='" + infoTambahan + "'" +
            ", intervensiMencegah='" + intervensiMencegah + "'" +
            '}';
    }
}
