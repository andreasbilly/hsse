/**
 * View Models used by Spring MVC REST controllers.
 */
package com.pertamina.hsseapplication.web.rest.vm;
