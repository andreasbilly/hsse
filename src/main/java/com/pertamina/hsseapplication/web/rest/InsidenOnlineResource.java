package com.pertamina.hsseapplication.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.pertamina.hsseapplication.domain.InsidenOnline;

import com.pertamina.hsseapplication.repository.InsidenOnlineRepository;
import com.pertamina.hsseapplication.web.rest.util.HeaderUtil;
import com.pertamina.hsseapplication.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing InsidenOnline.
 */
@RestController
@RequestMapping("/api")
public class InsidenOnlineResource {

    private final Logger log = LoggerFactory.getLogger(InsidenOnlineResource.class);

    private static final String ENTITY_NAME = "insidenOnline";
        
    private final InsidenOnlineRepository insidenOnlineRepository;

    public InsidenOnlineResource(InsidenOnlineRepository insidenOnlineRepository) {
        this.insidenOnlineRepository = insidenOnlineRepository;
    }

    /**
     * POST  /insiden-onlines : Create a new insidenOnline.
     *
     * @param insidenOnline the insidenOnline to create
     * @return the ResponseEntity with status 201 (Created) and with body the new insidenOnline, or with status 400 (Bad Request) if the insidenOnline has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/insiden-onlines")
    @Timed
    public ResponseEntity<InsidenOnline> createInsidenOnline(@RequestBody InsidenOnline insidenOnline) throws URISyntaxException {
        log.debug("REST request to save InsidenOnline : {}", insidenOnline);
        if (insidenOnline.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new insidenOnline cannot already have an ID")).body(null);
        }
        InsidenOnline result = insidenOnlineRepository.save(insidenOnline);
        return ResponseEntity.created(new URI("/api/insiden-onlines/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /insiden-onlines : Updates an existing insidenOnline.
     *
     * @param insidenOnline the insidenOnline to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated insidenOnline,
     * or with status 400 (Bad Request) if the insidenOnline is not valid,
     * or with status 500 (Internal Server Error) if the insidenOnline couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/insiden-onlines")
    @Timed
    public ResponseEntity<InsidenOnline> updateInsidenOnline(@RequestBody InsidenOnline insidenOnline) throws URISyntaxException {
        log.debug("REST request to update InsidenOnline : {}", insidenOnline);
        if (insidenOnline.getId() == null) {
            return createInsidenOnline(insidenOnline);
        }
        InsidenOnline result = insidenOnlineRepository.save(insidenOnline);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, insidenOnline.getId().toString()))
            .body(result);
    }

    /**
     * GET  /insiden-onlines : get all the insidenOnlines.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of insidenOnlines in body
     */
    @GetMapping("/insiden-onlines")
    @Timed
    public ResponseEntity<List<InsidenOnline>> getAllInsidenOnlines(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of InsidenOnlines");
        Page<InsidenOnline> page = insidenOnlineRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/insiden-onlines");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /insiden-onlines/:id : get the "id" insidenOnline.
     *
     * @param id the id of the insidenOnline to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the insidenOnline, or with status 404 (Not Found)
     */
    @GetMapping("/insiden-onlines/{id}")
    @Timed
    public ResponseEntity<InsidenOnline> getInsidenOnline(@PathVariable Long id) {
        log.debug("REST request to get InsidenOnline : {}", id);
        InsidenOnline insidenOnline = insidenOnlineRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(insidenOnline));
    }

    /**
     * DELETE  /insiden-onlines/:id : delete the "id" insidenOnline.
     *
     * @param id the id of the insidenOnline to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/insiden-onlines/{id}")
    @Timed
    public ResponseEntity<Void> deleteInsidenOnline(@PathVariable Long id) {
        log.debug("REST request to delete InsidenOnline : {}", id);
        insidenOnlineRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

}
