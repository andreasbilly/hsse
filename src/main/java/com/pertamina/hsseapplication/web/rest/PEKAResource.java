package com.pertamina.hsseapplication.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.pertamina.hsseapplication.domain.PEKA;

import com.pertamina.hsseapplication.repository.PEKARepository;
import com.pertamina.hsseapplication.web.rest.util.HeaderUtil;
import com.pertamina.hsseapplication.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing PEKA.
 */
@RestController
@RequestMapping("/api")
public class PEKAResource {

    private final Logger log = LoggerFactory.getLogger(PEKAResource.class);

    private static final String ENTITY_NAME = "pEKA";
        
    private final PEKARepository pEKARepository;

    public PEKAResource(PEKARepository pEKARepository) {
        this.pEKARepository = pEKARepository;
    }

    /**
     * POST  /p-ekas : Create a new pEKA.
     *
     * @param pEKA the pEKA to create
     * @return the ResponseEntity with status 201 (Created) and with body the new pEKA, or with status 400 (Bad Request) if the pEKA has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/p-ekas")
    @Timed
    public ResponseEntity<PEKA> createPEKA(@RequestBody PEKA pEKA) throws URISyntaxException {
        log.debug("REST request to save PEKA : {}", pEKA);
        if (pEKA.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new pEKA cannot already have an ID")).body(null);
        }
        PEKA result = pEKARepository.save(pEKA);
        return ResponseEntity.created(new URI("/api/p-ekas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /p-ekas : Updates an existing pEKA.
     *
     * @param pEKA the pEKA to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated pEKA,
     * or with status 400 (Bad Request) if the pEKA is not valid,
     * or with status 500 (Internal Server Error) if the pEKA couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/p-ekas")
    @Timed
    public ResponseEntity<PEKA> updatePEKA(@RequestBody PEKA pEKA) throws URISyntaxException {
        log.debug("REST request to update PEKA : {}", pEKA);
        if (pEKA.getId() == null) {
            return createPEKA(pEKA);
        }
        PEKA result = pEKARepository.save(pEKA);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, pEKA.getId().toString()))
            .body(result);
    }

    /**
     * GET  /p-ekas : get all the pEKAS.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of pEKAS in body
     */
    @GetMapping("/p-ekas")
    @Timed
    public ResponseEntity<List<PEKA>> getAllPEKAS(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of PEKAS");
        Page<PEKA> page = pEKARepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/p-ekas");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /p-ekas/:id : get the "id" pEKA.
     *
     * @param id the id of the pEKA to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the pEKA, or with status 404 (Not Found)
     */
    @GetMapping("/p-ekas/{id}")
    @Timed
    public ResponseEntity<PEKA> getPEKA(@PathVariable Long id) {
        log.debug("REST request to get PEKA : {}", id);
        PEKA pEKA = pEKARepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(pEKA));
    }

    /**
     * DELETE  /p-ekas/:id : delete the "id" pEKA.
     *
     * @param id the id of the pEKA to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/p-ekas/{id}")
    @Timed
    public ResponseEntity<Void> deletePEKA(@PathVariable Long id) {
        log.debug("REST request to delete PEKA : {}", id);
        pEKARepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

}
