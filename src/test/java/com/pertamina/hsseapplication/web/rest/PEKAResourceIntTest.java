package com.pertamina.hsseapplication.web.rest;

import com.pertamina.hsseapplication.HsseApplicationApp;

import com.pertamina.hsseapplication.domain.PEKA;
import com.pertamina.hsseapplication.repository.PEKARepository;
import com.pertamina.hsseapplication.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.pertamina.hsseapplication.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PEKAResource REST controller.
 *
 * @see PEKAResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = HsseApplicationApp.class)
public class PEKAResourceIntTest {

    private static final ZonedDateTime DEFAULT_DATE_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_INSIDEN = "AAAAAAAAAA";
    private static final String UPDATED_INSIDEN = "BBBBBBBBBB";

    private static final String DEFAULT_LOKASI = "AAAAAAAAAA";
    private static final String UPDATED_LOKASI = "BBBBBBBBBB";

    private static final String DEFAULT_POTENSI_BAHAYA = "AAAAAAAAAA";
    private static final String UPDATED_POTENSI_BAHAYA = "BBBBBBBBBB";

    private static final String DEFAULT_INTERVENSI = "AAAAAAAAAA";
    private static final String UPDATED_INTERVENSI = "BBBBBBBBBB";

    private static final String DEFAULT_HASIL_PENGAMATAN = "AAAAAAAAAA";
    private static final String UPDATED_HASIL_PENGAMATAN = "BBBBBBBBBB";

    private static final String DEFAULT_TINDAKAN_LANGSUNG = "AAAAAAAAAA";
    private static final String UPDATED_TINDAKAN_LANGSUNG = "BBBBBBBBBB";

    private static final String DEFAULT_INFO_TAMBAHAN = "AAAAAAAAAA";
    private static final String UPDATED_INFO_TAMBAHAN = "BBBBBBBBBB";

    private static final String DEFAULT_INTERVENSI_MENCEGAH = "AAAAAAAAAA";
    private static final String UPDATED_INTERVENSI_MENCEGAH = "BBBBBBBBBB";

    @Autowired
    private PEKARepository pEKARepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPEKAMockMvc;

    private PEKA pEKA;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PEKAResource pEKAResource = new PEKAResource(pEKARepository);
        this.restPEKAMockMvc = MockMvcBuilders.standaloneSetup(pEKAResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PEKA createEntity(EntityManager em) {
        PEKA pEKA = new PEKA()
            .dateTime(DEFAULT_DATE_TIME)
            .insiden(DEFAULT_INSIDEN)
            .lokasi(DEFAULT_LOKASI)
            .potensiBahaya(DEFAULT_POTENSI_BAHAYA)
            .intervensi(DEFAULT_INTERVENSI)
            .hasilPengamatan(DEFAULT_HASIL_PENGAMATAN)
            .tindakanLangsung(DEFAULT_TINDAKAN_LANGSUNG)
            .infoTambahan(DEFAULT_INFO_TAMBAHAN)
            .intervensiMencegah(DEFAULT_INTERVENSI_MENCEGAH);
        return pEKA;
    }

    @Before
    public void initTest() {
        pEKA = createEntity(em);
    }

    @Test
    @Transactional
    public void createPEKA() throws Exception {
        int databaseSizeBeforeCreate = pEKARepository.findAll().size();

        // Create the PEKA
        restPEKAMockMvc.perform(post("/api/p-ekas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pEKA)))
            .andExpect(status().isCreated());

        // Validate the PEKA in the database
        List<PEKA> pEKAList = pEKARepository.findAll();
        assertThat(pEKAList).hasSize(databaseSizeBeforeCreate + 1);
        PEKA testPEKA = pEKAList.get(pEKAList.size() - 1);
        assertThat(testPEKA.getDateTime()).isEqualTo(DEFAULT_DATE_TIME);
        assertThat(testPEKA.getInsiden()).isEqualTo(DEFAULT_INSIDEN);
        assertThat(testPEKA.getLokasi()).isEqualTo(DEFAULT_LOKASI);
        assertThat(testPEKA.getPotensiBahaya()).isEqualTo(DEFAULT_POTENSI_BAHAYA);
        assertThat(testPEKA.getIntervensi()).isEqualTo(DEFAULT_INTERVENSI);
        assertThat(testPEKA.getHasilPengamatan()).isEqualTo(DEFAULT_HASIL_PENGAMATAN);
        assertThat(testPEKA.getTindakanLangsung()).isEqualTo(DEFAULT_TINDAKAN_LANGSUNG);
        assertThat(testPEKA.getInfoTambahan()).isEqualTo(DEFAULT_INFO_TAMBAHAN);
        assertThat(testPEKA.getIntervensiMencegah()).isEqualTo(DEFAULT_INTERVENSI_MENCEGAH);
    }

    @Test
    @Transactional
    public void createPEKAWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = pEKARepository.findAll().size();

        // Create the PEKA with an existing ID
        pEKA.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPEKAMockMvc.perform(post("/api/p-ekas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pEKA)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<PEKA> pEKAList = pEKARepository.findAll();
        assertThat(pEKAList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllPEKAS() throws Exception {
        // Initialize the database
        pEKARepository.saveAndFlush(pEKA);

        // Get all the pEKAList
        restPEKAMockMvc.perform(get("/api/p-ekas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pEKA.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateTime").value(hasItem(sameInstant(DEFAULT_DATE_TIME))))
            .andExpect(jsonPath("$.[*].insiden").value(hasItem(DEFAULT_INSIDEN.toString())))
            .andExpect(jsonPath("$.[*].lokasi").value(hasItem(DEFAULT_LOKASI.toString())))
            .andExpect(jsonPath("$.[*].potensiBahaya").value(hasItem(DEFAULT_POTENSI_BAHAYA.toString())))
            .andExpect(jsonPath("$.[*].intervensi").value(hasItem(DEFAULT_INTERVENSI.toString())))
            .andExpect(jsonPath("$.[*].hasilPengamatan").value(hasItem(DEFAULT_HASIL_PENGAMATAN.toString())))
            .andExpect(jsonPath("$.[*].tindakanLangsung").value(hasItem(DEFAULT_TINDAKAN_LANGSUNG.toString())))
            .andExpect(jsonPath("$.[*].infoTambahan").value(hasItem(DEFAULT_INFO_TAMBAHAN.toString())))
            .andExpect(jsonPath("$.[*].intervensiMencegah").value(hasItem(DEFAULT_INTERVENSI_MENCEGAH.toString())));
    }

    @Test
    @Transactional
    public void getPEKA() throws Exception {
        // Initialize the database
        pEKARepository.saveAndFlush(pEKA);

        // Get the pEKA
        restPEKAMockMvc.perform(get("/api/p-ekas/{id}", pEKA.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(pEKA.getId().intValue()))
            .andExpect(jsonPath("$.dateTime").value(sameInstant(DEFAULT_DATE_TIME)))
            .andExpect(jsonPath("$.insiden").value(DEFAULT_INSIDEN.toString()))
            .andExpect(jsonPath("$.lokasi").value(DEFAULT_LOKASI.toString()))
            .andExpect(jsonPath("$.potensiBahaya").value(DEFAULT_POTENSI_BAHAYA.toString()))
            .andExpect(jsonPath("$.intervensi").value(DEFAULT_INTERVENSI.toString()))
            .andExpect(jsonPath("$.hasilPengamatan").value(DEFAULT_HASIL_PENGAMATAN.toString()))
            .andExpect(jsonPath("$.tindakanLangsung").value(DEFAULT_TINDAKAN_LANGSUNG.toString()))
            .andExpect(jsonPath("$.infoTambahan").value(DEFAULT_INFO_TAMBAHAN.toString()))
            .andExpect(jsonPath("$.intervensiMencegah").value(DEFAULT_INTERVENSI_MENCEGAH.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPEKA() throws Exception {
        // Get the pEKA
        restPEKAMockMvc.perform(get("/api/p-ekas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePEKA() throws Exception {
        // Initialize the database
        pEKARepository.saveAndFlush(pEKA);
        int databaseSizeBeforeUpdate = pEKARepository.findAll().size();

        // Update the pEKA
        PEKA updatedPEKA = pEKARepository.findOne(pEKA.getId());
        updatedPEKA
            .dateTime(UPDATED_DATE_TIME)
            .insiden(UPDATED_INSIDEN)
            .lokasi(UPDATED_LOKASI)
            .potensiBahaya(UPDATED_POTENSI_BAHAYA)
            .intervensi(UPDATED_INTERVENSI)
            .hasilPengamatan(UPDATED_HASIL_PENGAMATAN)
            .tindakanLangsung(UPDATED_TINDAKAN_LANGSUNG)
            .infoTambahan(UPDATED_INFO_TAMBAHAN)
            .intervensiMencegah(UPDATED_INTERVENSI_MENCEGAH);

        restPEKAMockMvc.perform(put("/api/p-ekas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPEKA)))
            .andExpect(status().isOk());

        // Validate the PEKA in the database
        List<PEKA> pEKAList = pEKARepository.findAll();
        assertThat(pEKAList).hasSize(databaseSizeBeforeUpdate);
        PEKA testPEKA = pEKAList.get(pEKAList.size() - 1);
        assertThat(testPEKA.getDateTime()).isEqualTo(UPDATED_DATE_TIME);
        assertThat(testPEKA.getInsiden()).isEqualTo(UPDATED_INSIDEN);
        assertThat(testPEKA.getLokasi()).isEqualTo(UPDATED_LOKASI);
        assertThat(testPEKA.getPotensiBahaya()).isEqualTo(UPDATED_POTENSI_BAHAYA);
        assertThat(testPEKA.getIntervensi()).isEqualTo(UPDATED_INTERVENSI);
        assertThat(testPEKA.getHasilPengamatan()).isEqualTo(UPDATED_HASIL_PENGAMATAN);
        assertThat(testPEKA.getTindakanLangsung()).isEqualTo(UPDATED_TINDAKAN_LANGSUNG);
        assertThat(testPEKA.getInfoTambahan()).isEqualTo(UPDATED_INFO_TAMBAHAN);
        assertThat(testPEKA.getIntervensiMencegah()).isEqualTo(UPDATED_INTERVENSI_MENCEGAH);
    }

    @Test
    @Transactional
    public void updateNonExistingPEKA() throws Exception {
        int databaseSizeBeforeUpdate = pEKARepository.findAll().size();

        // Create the PEKA

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPEKAMockMvc.perform(put("/api/p-ekas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pEKA)))
            .andExpect(status().isCreated());

        // Validate the PEKA in the database
        List<PEKA> pEKAList = pEKARepository.findAll();
        assertThat(pEKAList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePEKA() throws Exception {
        // Initialize the database
        pEKARepository.saveAndFlush(pEKA);
        int databaseSizeBeforeDelete = pEKARepository.findAll().size();

        // Get the pEKA
        restPEKAMockMvc.perform(delete("/api/p-ekas/{id}", pEKA.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PEKA> pEKAList = pEKARepository.findAll();
        assertThat(pEKAList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PEKA.class);
    }
}
