package com.pertamina.hsseapplication.web.rest;

import com.pertamina.hsseapplication.HsseApplicationApp;

import com.pertamina.hsseapplication.domain.InsidenOnline;
import com.pertamina.hsseapplication.repository.InsidenOnlineRepository;
import com.pertamina.hsseapplication.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.pertamina.hsseapplication.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the InsidenOnlineResource REST controller.
 *
 * @see InsidenOnlineResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = HsseApplicationApp.class)
public class InsidenOnlineResourceIntTest {

    private static final String DEFAULT_UBAH_KE = "AAAAAAAAAA";
    private static final String UPDATED_UBAH_KE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DITETAPKAN_TANGGAL = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DITETAPKAN_TANGGAL = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_MULAI_TERJADI = "AAAAAAAAAA";
    private static final String UPDATED_MULAI_TERJADI = "BBBBBBBBBB";

    private static final String DEFAULT_LOKASI_KECELAKAAN = "AAAAAAAAAA";
    private static final String UPDATED_LOKASI_KECELAKAAN = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_TANGGAL_KEJADIAN = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_TANGGAL_KEJADIAN = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NAMA_KORBAN = "AAAAAAAAAA";
    private static final String UPDATED_NAMA_KORBAN = "BBBBBBBBBB";

    private static final String DEFAULT_CIDERA_KERUSAKAN = "AAAAAAAAAA";
    private static final String UPDATED_CIDERA_KERUSAKAN = "BBBBBBBBBB";

    private static final String DEFAULT_DAMPAK_KERUGIAN = "AAAAAAAAAA";
    private static final String UPDATED_DAMPAK_KERUGIAN = "BBBBBBBBBB";

    private static final String DEFAULT_DEPARTMENT = "AAAAAAAAAA";
    private static final String UPDATED_DEPARTMENT = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_JAM = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_JAM = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_BAGIAN_TUBUH_TERLUKA = "AAAAAAAAAA";
    private static final String UPDATED_BAGIAN_TUBUH_TERLUKA = "BBBBBBBBBB";

    private static final String DEFAULT_PENYEBAB_CIDERA = "AAAAAAAAAA";
    private static final String UPDATED_PENYEBAB_CIDERA = "BBBBBBBBBB";

    private static final String DEFAULT_FREKUENSI_TERJADI = "AAAAAAAAAA";
    private static final String UPDATED_FREKUENSI_TERJADI = "BBBBBBBBBB";

    private static final String DEFAULT_BAGAIMANA_TERJADI = "AAAAAAAAAA";
    private static final String UPDATED_BAGAIMANA_TERJADI = "BBBBBBBBBB";

    private static final String DEFAULT_PENYEBAB_KEJADIAN_TINDAKAN = "AAAAAAAAAA";
    private static final String UPDATED_PENYEBAB_KEJADIAN_TINDAKAN = "BBBBBBBBBB";

    private static final String DEFAULT_PENYEBAB_FAKTOR_MANUSIA = "AAAAAAAAAA";
    private static final String UPDATED_PENYEBAB_FAKTOR_MANUSIA = "BBBBBBBBBB";

    private static final String DEFAULT_INTERVENSI_PENCEGAHAN = "AAAAAAAAAA";
    private static final String UPDATED_INTERVENSI_PENCEGAHAN = "BBBBBBBBBB";

    private static final String DEFAULT_TINDAKAN_MENGELIMINASI = "AAAAAAAAAA";
    private static final String UPDATED_TINDAKAN_MENGELIMINASI = "BBBBBBBBBB";

    @Autowired
    private InsidenOnlineRepository insidenOnlineRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restInsidenOnlineMockMvc;

    private InsidenOnline insidenOnline;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        InsidenOnlineResource insidenOnlineResource = new InsidenOnlineResource(insidenOnlineRepository);
        this.restInsidenOnlineMockMvc = MockMvcBuilders.standaloneSetup(insidenOnlineResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static InsidenOnline createEntity(EntityManager em) {
        InsidenOnline insidenOnline = new InsidenOnline()
            .ubahKe(DEFAULT_UBAH_KE)
            .ditetapkanTanggal(DEFAULT_DITETAPKAN_TANGGAL)
            .mulaiTerjadi(DEFAULT_MULAI_TERJADI)
            .lokasiKecelakaan(DEFAULT_LOKASI_KECELAKAAN)
            .tanggalKejadian(DEFAULT_TANGGAL_KEJADIAN)
            .namaKorban(DEFAULT_NAMA_KORBAN)
            .cideraKerusakan(DEFAULT_CIDERA_KERUSAKAN)
            .dampakKerugian(DEFAULT_DAMPAK_KERUGIAN)
            .department(DEFAULT_DEPARTMENT)
            .jam(DEFAULT_JAM)
            .bagianTubuhTerluka(DEFAULT_BAGIAN_TUBUH_TERLUKA)
            .penyebabCidera(DEFAULT_PENYEBAB_CIDERA)
            .frekuensiTerjadi(DEFAULT_FREKUENSI_TERJADI)
            .bagaimanaTerjadi(DEFAULT_BAGAIMANA_TERJADI)
            .penyebabKejadianTindakan(DEFAULT_PENYEBAB_KEJADIAN_TINDAKAN)
            .penyebabFaktorManusia(DEFAULT_PENYEBAB_FAKTOR_MANUSIA)
            .intervensiPencegahan(DEFAULT_INTERVENSI_PENCEGAHAN)
            .tindakanMengeliminasi(DEFAULT_TINDAKAN_MENGELIMINASI);
        return insidenOnline;
    }

    @Before
    public void initTest() {
        insidenOnline = createEntity(em);
    }

    @Test
    @Transactional
    public void createInsidenOnline() throws Exception {
        int databaseSizeBeforeCreate = insidenOnlineRepository.findAll().size();

        // Create the InsidenOnline
        restInsidenOnlineMockMvc.perform(post("/api/insiden-onlines")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(insidenOnline)))
            .andExpect(status().isCreated());

        // Validate the InsidenOnline in the database
        List<InsidenOnline> insidenOnlineList = insidenOnlineRepository.findAll();
        assertThat(insidenOnlineList).hasSize(databaseSizeBeforeCreate + 1);
        InsidenOnline testInsidenOnline = insidenOnlineList.get(insidenOnlineList.size() - 1);
        assertThat(testInsidenOnline.getUbahKe()).isEqualTo(DEFAULT_UBAH_KE);
        assertThat(testInsidenOnline.getDitetapkanTanggal()).isEqualTo(DEFAULT_DITETAPKAN_TANGGAL);
        assertThat(testInsidenOnline.getMulaiTerjadi()).isEqualTo(DEFAULT_MULAI_TERJADI);
        assertThat(testInsidenOnline.getLokasiKecelakaan()).isEqualTo(DEFAULT_LOKASI_KECELAKAAN);
        assertThat(testInsidenOnline.getTanggalKejadian()).isEqualTo(DEFAULT_TANGGAL_KEJADIAN);
        assertThat(testInsidenOnline.getNamaKorban()).isEqualTo(DEFAULT_NAMA_KORBAN);
        assertThat(testInsidenOnline.getCideraKerusakan()).isEqualTo(DEFAULT_CIDERA_KERUSAKAN);
        assertThat(testInsidenOnline.getDampakKerugian()).isEqualTo(DEFAULT_DAMPAK_KERUGIAN);
        assertThat(testInsidenOnline.getDepartment()).isEqualTo(DEFAULT_DEPARTMENT);
        assertThat(testInsidenOnline.getJam()).isEqualTo(DEFAULT_JAM);
        assertThat(testInsidenOnline.getBagianTubuhTerluka()).isEqualTo(DEFAULT_BAGIAN_TUBUH_TERLUKA);
        assertThat(testInsidenOnline.getPenyebabCidera()).isEqualTo(DEFAULT_PENYEBAB_CIDERA);
        assertThat(testInsidenOnline.getFrekuensiTerjadi()).isEqualTo(DEFAULT_FREKUENSI_TERJADI);
        assertThat(testInsidenOnline.getBagaimanaTerjadi()).isEqualTo(DEFAULT_BAGAIMANA_TERJADI);
        assertThat(testInsidenOnline.getPenyebabKejadianTindakan()).isEqualTo(DEFAULT_PENYEBAB_KEJADIAN_TINDAKAN);
        assertThat(testInsidenOnline.getPenyebabFaktorManusia()).isEqualTo(DEFAULT_PENYEBAB_FAKTOR_MANUSIA);
        assertThat(testInsidenOnline.getIntervensiPencegahan()).isEqualTo(DEFAULT_INTERVENSI_PENCEGAHAN);
        assertThat(testInsidenOnline.getTindakanMengeliminasi()).isEqualTo(DEFAULT_TINDAKAN_MENGELIMINASI);
    }

    @Test
    @Transactional
    public void createInsidenOnlineWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = insidenOnlineRepository.findAll().size();

        // Create the InsidenOnline with an existing ID
        insidenOnline.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restInsidenOnlineMockMvc.perform(post("/api/insiden-onlines")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(insidenOnline)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<InsidenOnline> insidenOnlineList = insidenOnlineRepository.findAll();
        assertThat(insidenOnlineList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllInsidenOnlines() throws Exception {
        // Initialize the database
        insidenOnlineRepository.saveAndFlush(insidenOnline);

        // Get all the insidenOnlineList
        restInsidenOnlineMockMvc.perform(get("/api/insiden-onlines?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(insidenOnline.getId().intValue())))
            .andExpect(jsonPath("$.[*].ubahKe").value(hasItem(DEFAULT_UBAH_KE.toString())))
            .andExpect(jsonPath("$.[*].ditetapkanTanggal").value(hasItem(DEFAULT_DITETAPKAN_TANGGAL.toString())))
            .andExpect(jsonPath("$.[*].mulaiTerjadi").value(hasItem(DEFAULT_MULAI_TERJADI.toString())))
            .andExpect(jsonPath("$.[*].lokasiKecelakaan").value(hasItem(DEFAULT_LOKASI_KECELAKAAN.toString())))
            .andExpect(jsonPath("$.[*].tanggalKejadian").value(hasItem(DEFAULT_TANGGAL_KEJADIAN.toString())))
            .andExpect(jsonPath("$.[*].namaKorban").value(hasItem(DEFAULT_NAMA_KORBAN.toString())))
            .andExpect(jsonPath("$.[*].cideraKerusakan").value(hasItem(DEFAULT_CIDERA_KERUSAKAN.toString())))
            .andExpect(jsonPath("$.[*].dampakKerugian").value(hasItem(DEFAULT_DAMPAK_KERUGIAN.toString())))
            .andExpect(jsonPath("$.[*].department").value(hasItem(DEFAULT_DEPARTMENT.toString())))
            .andExpect(jsonPath("$.[*].jam").value(hasItem(sameInstant(DEFAULT_JAM))))
            .andExpect(jsonPath("$.[*].bagianTubuhTerluka").value(hasItem(DEFAULT_BAGIAN_TUBUH_TERLUKA.toString())))
            .andExpect(jsonPath("$.[*].penyebabCidera").value(hasItem(DEFAULT_PENYEBAB_CIDERA.toString())))
            .andExpect(jsonPath("$.[*].frekuensiTerjadi").value(hasItem(DEFAULT_FREKUENSI_TERJADI.toString())))
            .andExpect(jsonPath("$.[*].bagaimanaTerjadi").value(hasItem(DEFAULT_BAGAIMANA_TERJADI.toString())))
            .andExpect(jsonPath("$.[*].penyebabKejadianTindakan").value(hasItem(DEFAULT_PENYEBAB_KEJADIAN_TINDAKAN.toString())))
            .andExpect(jsonPath("$.[*].penyebabFaktorManusia").value(hasItem(DEFAULT_PENYEBAB_FAKTOR_MANUSIA.toString())))
            .andExpect(jsonPath("$.[*].intervensiPencegahan").value(hasItem(DEFAULT_INTERVENSI_PENCEGAHAN.toString())))
            .andExpect(jsonPath("$.[*].tindakanMengeliminasi").value(hasItem(DEFAULT_TINDAKAN_MENGELIMINASI.toString())));
    }

    @Test
    @Transactional
    public void getInsidenOnline() throws Exception {
        // Initialize the database
        insidenOnlineRepository.saveAndFlush(insidenOnline);

        // Get the insidenOnline
        restInsidenOnlineMockMvc.perform(get("/api/insiden-onlines/{id}", insidenOnline.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(insidenOnline.getId().intValue()))
            .andExpect(jsonPath("$.ubahKe").value(DEFAULT_UBAH_KE.toString()))
            .andExpect(jsonPath("$.ditetapkanTanggal").value(DEFAULT_DITETAPKAN_TANGGAL.toString()))
            .andExpect(jsonPath("$.mulaiTerjadi").value(DEFAULT_MULAI_TERJADI.toString()))
            .andExpect(jsonPath("$.lokasiKecelakaan").value(DEFAULT_LOKASI_KECELAKAAN.toString()))
            .andExpect(jsonPath("$.tanggalKejadian").value(DEFAULT_TANGGAL_KEJADIAN.toString()))
            .andExpect(jsonPath("$.namaKorban").value(DEFAULT_NAMA_KORBAN.toString()))
            .andExpect(jsonPath("$.cideraKerusakan").value(DEFAULT_CIDERA_KERUSAKAN.toString()))
            .andExpect(jsonPath("$.dampakKerugian").value(DEFAULT_DAMPAK_KERUGIAN.toString()))
            .andExpect(jsonPath("$.department").value(DEFAULT_DEPARTMENT.toString()))
            .andExpect(jsonPath("$.jam").value(sameInstant(DEFAULT_JAM)))
            .andExpect(jsonPath("$.bagianTubuhTerluka").value(DEFAULT_BAGIAN_TUBUH_TERLUKA.toString()))
            .andExpect(jsonPath("$.penyebabCidera").value(DEFAULT_PENYEBAB_CIDERA.toString()))
            .andExpect(jsonPath("$.frekuensiTerjadi").value(DEFAULT_FREKUENSI_TERJADI.toString()))
            .andExpect(jsonPath("$.bagaimanaTerjadi").value(DEFAULT_BAGAIMANA_TERJADI.toString()))
            .andExpect(jsonPath("$.penyebabKejadianTindakan").value(DEFAULT_PENYEBAB_KEJADIAN_TINDAKAN.toString()))
            .andExpect(jsonPath("$.penyebabFaktorManusia").value(DEFAULT_PENYEBAB_FAKTOR_MANUSIA.toString()))
            .andExpect(jsonPath("$.intervensiPencegahan").value(DEFAULT_INTERVENSI_PENCEGAHAN.toString()))
            .andExpect(jsonPath("$.tindakanMengeliminasi").value(DEFAULT_TINDAKAN_MENGELIMINASI.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingInsidenOnline() throws Exception {
        // Get the insidenOnline
        restInsidenOnlineMockMvc.perform(get("/api/insiden-onlines/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateInsidenOnline() throws Exception {
        // Initialize the database
        insidenOnlineRepository.saveAndFlush(insidenOnline);
        int databaseSizeBeforeUpdate = insidenOnlineRepository.findAll().size();

        // Update the insidenOnline
        InsidenOnline updatedInsidenOnline = insidenOnlineRepository.findOne(insidenOnline.getId());
        updatedInsidenOnline
            .ubahKe(UPDATED_UBAH_KE)
            .ditetapkanTanggal(UPDATED_DITETAPKAN_TANGGAL)
            .mulaiTerjadi(UPDATED_MULAI_TERJADI)
            .lokasiKecelakaan(UPDATED_LOKASI_KECELAKAAN)
            .tanggalKejadian(UPDATED_TANGGAL_KEJADIAN)
            .namaKorban(UPDATED_NAMA_KORBAN)
            .cideraKerusakan(UPDATED_CIDERA_KERUSAKAN)
            .dampakKerugian(UPDATED_DAMPAK_KERUGIAN)
            .department(UPDATED_DEPARTMENT)
            .jam(UPDATED_JAM)
            .bagianTubuhTerluka(UPDATED_BAGIAN_TUBUH_TERLUKA)
            .penyebabCidera(UPDATED_PENYEBAB_CIDERA)
            .frekuensiTerjadi(UPDATED_FREKUENSI_TERJADI)
            .bagaimanaTerjadi(UPDATED_BAGAIMANA_TERJADI)
            .penyebabKejadianTindakan(UPDATED_PENYEBAB_KEJADIAN_TINDAKAN)
            .penyebabFaktorManusia(UPDATED_PENYEBAB_FAKTOR_MANUSIA)
            .intervensiPencegahan(UPDATED_INTERVENSI_PENCEGAHAN)
            .tindakanMengeliminasi(UPDATED_TINDAKAN_MENGELIMINASI);

        restInsidenOnlineMockMvc.perform(put("/api/insiden-onlines")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedInsidenOnline)))
            .andExpect(status().isOk());

        // Validate the InsidenOnline in the database
        List<InsidenOnline> insidenOnlineList = insidenOnlineRepository.findAll();
        assertThat(insidenOnlineList).hasSize(databaseSizeBeforeUpdate);
        InsidenOnline testInsidenOnline = insidenOnlineList.get(insidenOnlineList.size() - 1);
        assertThat(testInsidenOnline.getUbahKe()).isEqualTo(UPDATED_UBAH_KE);
        assertThat(testInsidenOnline.getDitetapkanTanggal()).isEqualTo(UPDATED_DITETAPKAN_TANGGAL);
        assertThat(testInsidenOnline.getMulaiTerjadi()).isEqualTo(UPDATED_MULAI_TERJADI);
        assertThat(testInsidenOnline.getLokasiKecelakaan()).isEqualTo(UPDATED_LOKASI_KECELAKAAN);
        assertThat(testInsidenOnline.getTanggalKejadian()).isEqualTo(UPDATED_TANGGAL_KEJADIAN);
        assertThat(testInsidenOnline.getNamaKorban()).isEqualTo(UPDATED_NAMA_KORBAN);
        assertThat(testInsidenOnline.getCideraKerusakan()).isEqualTo(UPDATED_CIDERA_KERUSAKAN);
        assertThat(testInsidenOnline.getDampakKerugian()).isEqualTo(UPDATED_DAMPAK_KERUGIAN);
        assertThat(testInsidenOnline.getDepartment()).isEqualTo(UPDATED_DEPARTMENT);
        assertThat(testInsidenOnline.getJam()).isEqualTo(UPDATED_JAM);
        assertThat(testInsidenOnline.getBagianTubuhTerluka()).isEqualTo(UPDATED_BAGIAN_TUBUH_TERLUKA);
        assertThat(testInsidenOnline.getPenyebabCidera()).isEqualTo(UPDATED_PENYEBAB_CIDERA);
        assertThat(testInsidenOnline.getFrekuensiTerjadi()).isEqualTo(UPDATED_FREKUENSI_TERJADI);
        assertThat(testInsidenOnline.getBagaimanaTerjadi()).isEqualTo(UPDATED_BAGAIMANA_TERJADI);
        assertThat(testInsidenOnline.getPenyebabKejadianTindakan()).isEqualTo(UPDATED_PENYEBAB_KEJADIAN_TINDAKAN);
        assertThat(testInsidenOnline.getPenyebabFaktorManusia()).isEqualTo(UPDATED_PENYEBAB_FAKTOR_MANUSIA);
        assertThat(testInsidenOnline.getIntervensiPencegahan()).isEqualTo(UPDATED_INTERVENSI_PENCEGAHAN);
        assertThat(testInsidenOnline.getTindakanMengeliminasi()).isEqualTo(UPDATED_TINDAKAN_MENGELIMINASI);
    }

    @Test
    @Transactional
    public void updateNonExistingInsidenOnline() throws Exception {
        int databaseSizeBeforeUpdate = insidenOnlineRepository.findAll().size();

        // Create the InsidenOnline

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restInsidenOnlineMockMvc.perform(put("/api/insiden-onlines")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(insidenOnline)))
            .andExpect(status().isCreated());

        // Validate the InsidenOnline in the database
        List<InsidenOnline> insidenOnlineList = insidenOnlineRepository.findAll();
        assertThat(insidenOnlineList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteInsidenOnline() throws Exception {
        // Initialize the database
        insidenOnlineRepository.saveAndFlush(insidenOnline);
        int databaseSizeBeforeDelete = insidenOnlineRepository.findAll().size();

        // Get the insidenOnline
        restInsidenOnlineMockMvc.perform(delete("/api/insiden-onlines/{id}", insidenOnline.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<InsidenOnline> insidenOnlineList = insidenOnlineRepository.findAll();
        assertThat(insidenOnlineList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(InsidenOnline.class);
    }
}
